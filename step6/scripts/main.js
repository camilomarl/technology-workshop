function Notes(){
  this.checkSetup();
  this.initFirebase();
  this.signInButton = document.getElementById('sign-in');
  this.signInButton.addEventListener('click', this.signIn.bind(this));

  this.noteForm = document.getElementById('note-form');
  this.noteInput = document.getElementById('note');
  this.noteContainer = document.getElementById('note-container');

  this.submitImageButton = document.getElementById('save-image');
  this.imageForm = document.getElementById('image-form');
  this.fileInput = document.getElementById('file');


  this.noteForm.addEventListener('submit', this.storeData.bind(this));
  this.imageForm.addEventListener('submit', this.storeImage.bind(this));
  this.fileInput.addEventListener('change', this.storeImage.bind(this));
  
  this.notesRef = this.database.ref('notes');

  this.user = null;
}

Notes.prototype.checkSetup = function(first_argument) {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('Firebase no esta configurado');
  }
};


Notes.prototype.initFirebase = function(first_argument) {
  this.auth = firebase.auth();
  this.database = firebase.database();
  this.storage = firebase.storage();
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

Notes.prototype.signIn = function() {
  // Sign in Firebase using popup auth and Google as the identity provider.
  var provider = new firebase.auth.GoogleAuthProvider();
  this.auth.signInWithPopup(provider);
};

Notes.prototype.signOut = function() {
  // Sign out of Firebase.
  this.auth.signOut();
};

Notes.prototype.onAuthStateChanged = function(user) {
  console.log(user);
  if(user){
    this.user = user;
    this.loadData();  
  }
  
};

Notes.prototype.storeData = function(event) {
  event.preventDefault();

    this.notesRef.push({
      text: this.noteInput.value

    }).then(function(response) {
      console.log(response)
    }).catch(function(error) {
      console.error('Error escribiendo en  Firebase', error);
    });

};

Notes.prototype.storeImage = function(event) {
  event.preventDefault();
  var self = this;

  var file = event.target.files[0];

  var filePath = file.name;
  return this.storage.ref(filePath).put(file).then(function(snapshot) {
    var imageUrl = snapshot.metadata.fullPath;
    console.log(imageUrl);
    return self.notesRef.push({
      image: imageUrl
    }).then(function(response) {
      console.log(response)
    }).catch(function(error) {
      console.error('Error escribiendo en  Firebase', error);
    });
  });
};

Notes.prototype.loadData = function() {

  var self = this;
  var displayNote = function(data) {
    var noteObject = data.val();
    var noteId = data.key;
    var noteText = noteObject.text;
    var noteHTML = document.getElementById(noteId);
  
    if (!noteHTML) {
      noteHTML = document.createElement('div');
      noteHTML.setAttribute('id', noteId);
      self.noteContainer.appendChild(noteHTML)
    }

    noteHTML.textContent = noteText;
    
  };

  this.notesRef.limitToLast(12).on('child_added', displayNote);
  this.notesRef.limitToLast(12).on('child_changed', displayNote);

};



window.onload = function(){
  window.notes = new Notes();
}