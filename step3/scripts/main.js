function Notes(){
  this.checkSetup();
  this.initFirebase();
  this.signInButton = document.getElementById('sign-in');
  this.signInButton.addEventListener('click', this.signIn.bind(this));
}

Notes.prototype.checkSetup = function(first_argument) {
  if (!window.firebase || !(firebase.app instanceof Function) || !firebase.app().options) {
    window.alert('Firebase no esta configurado');
  }
};


Notes.prototype.initFirebase = function(first_argument) {
  this.auth = firebase.auth();
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};

Notes.prototype.signIn = function() {
  // Sign in Firebase using popup auth and Google as the identity provider.
  var provider = new firebase.auth.GoogleAuthProvider();
  this.auth.signInWithPopup(provider);
};

Notes.prototype.signOut = function() {
  // Sign out of Firebase.
  this.auth.signOut();
};

Notes.prototype.onAuthStateChanged = function(user) {
  console.log(user);
};




window.onload = function(){
  window.notes = new Notes();
}