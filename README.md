# Workshop de Tecnología #

Integrando Plataformas para acelerar el proceso de desarrollo: Caso de Estudio - Google Firebase 


### Prerrequisitos ###

* Tener instalado node.js https://nodejs.org/en/download/ 
* Un IDE como sublime https://www.sublimetext.com/3
* Acceso a consola/terminal/power shell
* Google Chrome
* Crear una cuenta en firebase https://console.firebase.google.com

### Objetivos ###

Aprender el uso básico de firebase y varios de sus APIs como son:

* Firebase Real Time Database
* Firebase Auth
* Firebase Cloud Storage
* Firebase Static Hosting
* Firebase Cloud Messaging

### Contenido ###

Este repositorio tiene las carpetas que van guiando el contenido del workshop de tecnologia de 10xU

Cada carpeta tiene el siguiente contenido:

* Step1: Hola Mundo
* Step2: Estructura
* Step3: Autenticacion
* Step4: Guardando info en firebase realtime database
* Step5: Leyendo info de firebase realtime database
* Step6: Subiendo imagenes a firebase storage
* Step7: Realtime notifications
* Public: Ejercicio completo

